﻿using Autofac;
using Autofac.Integration.WebApi;
using PerfectPattern.DAL.DbFactory;
using PerfectPattern.DAL.Repositories;
using System.Reflection;

namespace PerfectPattern.API.IoC
{
    public static class ContainerConfig
    {
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();

            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<DbFactory>().As<IDbFactory>()
                .WithParameter("connectionString", "DB");
            builder.RegisterType<StudentRepository>().As<IStudentRepository>();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();

            return container;
        }
    }
}
