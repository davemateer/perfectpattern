﻿namespace PerfectPattern.Common.Models.DTO
{
    public class StudentDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
