﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NPoco;
using PerfectPattern.DAL.DbFactory;
using PerfectPattern.DAL.Repositories;
using PerfectPattern.DAL.Models.Entities;

namespace PerfectPattern.Test.Repositories
{
    [TestClass]
    public class StudentRepositoryTest
    {
        private Mock<IDbFactory> _mockDbFactory;
        private Mock<IDatabase> _mockDatabase;

        private IStudentRepository _studentRepository;

        [TestInitialize]
        public void Setup()
        {
            _mockDatabase = new Mock<IDatabase>();
            _mockDatabase.VerifyAll();

            _mockDbFactory = new Mock<IDbFactory>();

            _mockDbFactory.Setup(x => x.GetConnection())
                .Returns(_mockDatabase.Object);

            _studentRepository = new StudentRepository(_mockDbFactory.Object);
        }

        [TestMethod]
        public void GetAllStudentsTest()
        {
            _studentRepository.GetAllStudents();

            _mockDatabase.Verify(x => x.Fetch<Student>(), Times.Once);
        }

        [TestMethod]
        public void GetSingleStudentTest()
        {
            int studentID = 1;

            _studentRepository.GetStudentByID(1);

            _mockDatabase.Verify(x => x.SingleById<Student>(studentID), Times.Once);
        }

        [TestMethod]
        public void InsertStudentTest()
        {
            var newStudent = new Student
            {
                ID = 2,
                Name = "Conor McGregor",
                Age = 30
            };

            _studentRepository.AddStudent(newStudent);

            _mockDatabase.Verify(x => x.Insert(newStudent), Times.Once);
        }
    }
}
