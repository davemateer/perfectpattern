﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PerfectPattern.API.Controllers;
using PerfectPattern.Common.Models.DTO;
using PerfectPattern.DAL.Models.Entities;
using PerfectPattern.DAL.Repositories;
using System.Collections.Generic;

namespace PerfectPattern.Test.Controllers
{
    [TestClass]
    public class StudentControllerTest
    {
        private Mock<IStudentRepository> _repo;
        private StudentController _studentController;

        [TestInitialize]
        public void Setup()
        {
            var inMemoryStudentList = new List<Student>
            {
                new Student
                {
                    ID = 1,
                    Name = "John Doe",
                    Age = 28
                }
            };

            _repo = new Mock<IStudentRepository>();

            _repo.Setup(x => x.GetAllStudents()).Returns(inMemoryStudentList);

            _repo.Setup(x => x.GetStudentByID(It.IsAny<int>())).Returns(inMemoryStudentList[0]);

            _studentController = new StudentController(_repo.Object);
        }

        [TestMethod]
        public void GetAllStudentsTest()
        {
            var students = _studentController.GetAllStudents();

            _repo.Verify(x => x.GetAllStudents(), Times.Once);
            Assert.IsInstanceOfType(students, typeof(List<StudentDTO>), "Return type is not correct");
            Assert.AreEqual(1, students.Count, "Student count is not correct");
        }

        [TestMethod]
        public void GetSingleStudentTest()
        {
            int studentID = 1;

            var student = _studentController.GetStudentByID(studentID);

            _repo.Verify(x => x.GetStudentByID(studentID), Times.Once);
            Assert.IsInstanceOfType(student, typeof(StudentDTO), "Return type is not correct");
            Assert.AreEqual("John Doe", student.Name, "Student name is not correct");
            Assert.AreEqual(28, student.Age, "Student age is not correct");
        }
    }
}
