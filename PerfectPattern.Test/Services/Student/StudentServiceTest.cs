﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PerfectPattern.BLL.Services;
using PerfectPattern.Common.Models.DTO;
using PerfectPattern.DAL.Repositories;
using System.Collections.Generic;
using PerfectPattern.DAL.Models.Entities;

namespace PerfectPattern.Test.Services
{
    [TestClass]
    public class StudentServiceTest
    {
        private Mock<IStudentRepository> _mockStudentRepository;

        // System Under Test (SUT)
        private StudentService _studentService;

        [TestInitialize]
        public void Setup()
        {
            List<Student> inMemoryStudentList = new List<Student>
            {
                new Student
                {
                    ID = 1,
                    Name = "John Doe",
                    Age = 28
                }
            };

            _mockStudentRepository = new Mock<IStudentRepository>();

            _mockStudentRepository.Setup(x => x.GetAllStudents())
                .Returns(inMemoryStudentList);

            _mockStudentRepository.Setup(x => x.GetStudentByID(It.IsAny<int>()))
                .Returns(inMemoryStudentList[0]);

            _mockStudentRepository.Setup(x => x.AddStudent(It.IsAny<Student>()))
                .Callback((Student student) => inMemoryStudentList.Add(student));

            _studentService = new StudentService(_mockStudentRepository.Object);
        }

        [TestMethod]
        public void GetAllStudentsTest()
        {
            var students = _studentService.GetAllStudents();

            _mockStudentRepository.Verify(x => x.GetAllStudents(), Times.Once);
            Assert.IsInstanceOfType(students, typeof(List<StudentDTO>), "Return type is not correct");
            Assert.AreEqual(1, students.Count, "Student count is not correct");
        }

        [TestMethod]
        public void GetSingleStudentTest()
        {
            int studentID = 1;

            var student = _studentService.GetStudentByID(studentID);

            _mockStudentRepository.Verify(x => x.GetStudentByID(studentID), Times.Once);
            Assert.IsInstanceOfType(student, typeof(StudentDTO), "Return type is not correct");
            Assert.AreEqual("John Doe", student.Name, "Student name is not correct");
            Assert.AreEqual(28, student.Age, "Student age is not correct");
        }

        [TestMethod]
        public void InsertStudentTest()
        {
            StudentDTO newStudent = new StudentDTO
            {
                ID = 2,
                Name = "Conor McGregor",
                Age = 30
            };

            _studentService.AddStudent(newStudent);

            _mockStudentRepository.Verify(x => x.AddStudent(It.IsAny<Student>()), Times.Once);
            var students = _studentService.GetAllStudents();
            Assert.AreEqual(2, students.Count, "Student count is not correct");
        }
    }
}
