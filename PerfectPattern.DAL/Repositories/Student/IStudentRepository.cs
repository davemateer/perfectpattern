﻿using System.Collections.Generic;
using PerfectPattern.DAL.Models.Entities;

namespace PerfectPattern.DAL.Repositories
{
    public interface IStudentRepository
    {
        List<Student> GetAllStudents();
        Student GetStudentByID(int id);
        void AddStudent(Student student);
    }
}
