﻿using PerfectPattern.DAL.DbFactory;
using System.Collections.Generic;
using PerfectPattern.DAL.Models.Entities;

namespace PerfectPattern.DAL.Repositories
{
    public class StudentRepository : IStudentRepository
    {
        private readonly IDbFactory _dbFactory;

        public StudentRepository(IDbFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }

        public List<Student> GetAllStudents()
        {
            using (var db = _dbFactory.GetConnection())
            {
                return db.Fetch<Student>();
            }
        }

        public Student GetStudentByID(int id)
        {
            using (var db = _dbFactory.GetConnection())
            {
                return db.SingleById<Student>(id);
            }
        }

        public void AddStudent(Student student)
        {
            using (var db = _dbFactory.GetConnection())
            {
                db.Insert(student);
            }
        }
    }
}