﻿using NPoco;

namespace PerfectPattern.DAL.Models.Entities
{
    [TableName("Students")]
    public class Student
    {
        [Column("ID")]
        public int ID { get; set; }
        [Column("Name")]
        public string Name { get; set; }
        [Column("Age")]
        public int Age { get; set; }
    }
}
