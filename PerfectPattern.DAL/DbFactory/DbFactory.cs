﻿using NPoco;

namespace PerfectPattern.DAL.DbFactory
{
    public class DbFactory : IDbFactory
    {
        private readonly string _connectionString;

        public DbFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IDatabase GetConnection()
        {
            return new Database(_connectionString);
        }
    }
}
